import 'package:flutter/material.dart';

void main() {
  runApp(new RubberbandApp());
}

class RubberbandApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Rubberband Demo',
      theme: new ThemeData(primarySwatch: Colors.blue),
      home: new Scaffold(
        appBar: new AppBar(title: const Text('Rubberband Demo')),
        body: new Container(
          decoration: new BoxDecoration(
            color: Colors.white,
            border: new Border.all(
              color: Colors.black,
              width: 2.0,
            ),
          ),
          child: new Center(child: new RubberbandControl()),
        ),
      ),
    );
  }
}

class RubberbandControl extends StatefulWidget {

  const RubberbandControl({Key key}) : super(key: key);

  @override
  RubberbandControlState createState() => new RubberbandControlState();
}

class RubberbandControlState extends State<RubberbandControl> {
  Offset start, current;

  // TODO: Picture retainedPicture of all the retained graphics
  // TODO: Variable of retained graphics, index for efficient hit detect, etc.

  // Needed for input focus?
  GlobalKey _painterKey = new GlobalKey();

  Offset _globalToLocal(Offset globalOffset) {
    final RenderBox referenceBox = context.findRenderObject();
    return referenceBox.globalToLocal(globalOffset);
  }

  void _onPanStart(DragStartDetails details) {
    start = _globalToLocal(details.globalPosition);
  }

  void _onPanUpdate(DragUpdateDetails details) {
    Offset position = _globalToLocal(details.globalPosition);
    setState(() => current = position);
  }

  void _onPanEnd(DragEndDetails details) {
    // TODO: create a retained graphical object here using start and current
    setState(() {
      start = null;
      current = null;
    });
  }

  Size get _currentSize {
    if (start == null || current == null) return null;
    return new Size(
        (start.dx - current.dx).abs(), (start.dy - current.dy).abs());
  }

  @override
  Widget build(BuildContext context) {
    var boxConstraints = new BoxConstraints.expand();
    return new ConstrainedBox(
      constraints: boxConstraints,
      child: new GestureDetector(
        behavior: HitTestBehavior.opaque,
        onPanStart: _onPanStart,
        onPanUpdate: _onPanUpdate,
        onPanEnd: _onPanEnd,
        child: new CustomPaint(
          size: _currentSize ?? new Size(boxConstraints.maxWidth, boxConstraints.maxHeight),
          painter: new RubberbandPainter(start, current),
        ),
      ),
    );
  }
}

class RubberbandPainter extends CustomPainter {
  final Offset start, end;

  RubberbandPainter(this.start, this.end);

  @override
  void paint(Canvas canvas, Size size) {
    // TODO: when painting is complete, retain a picture for the next rubberband cycle
    // TODO: for now nothing is retained, only the rubberband is drawn from start to end

    // TODO: create PictureRecorder and another canvas to do the recording
    // TODO: Grab the picture
    // TODO: Draw the picture in this painter's canvas
    // TODO: Save the picture for the next cycle
    // TODO: When to destroy the picture? (it should be part of the State

    if (start == null || end == null) return null;

    canvas.drawColor(Colors.grey, BlendMode.src);
    Paint paint = new Paint()
      ..color = Colors.black
      ..strokeWidth = 3.0;
    canvas.drawLine(start, end, paint);
  }

  @override
  bool shouldRepaint(RubberbandPainter other) => start != other.start || end != other.end;
}

